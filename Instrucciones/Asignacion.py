from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo


class Asignacion(Instruccion):
    def __init__(self, fila, columna, identificador, expresion):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.expresion = expresion

    def ejecutar(self, entorno: Entorno):
        valor = self.expresion.ejecutar(entorno)

        variable: Simbolo = entorno.buscarSimbolo(self.identificador)
    
        
        if variable != None:

            simbolo = entorno.editarSimbolo(self.identificador, Simbolo(variable.tipo, self.identificador,
                                                                        valor, self.fila))
           
            # print(simbolo)
     