from Interface.Instruccion import Instruccion
from Expresiones.Identificador import Identificador

import webbrowser
class Tsimbolos(Instruccion):
    def __init__(self, fila, columna,nombreArchivo):
        self.fila = fila
        self.columna = columna
        self.nombreArchivo =  nombreArchivo

    def ejecutar(self, entorno):
        actual = entorno
        nombre = ""
        if isinstance(self.nombreArchivo, Identificador):
            nombre = self.nombreArchivo.ejecutar(entorno)+'.html'
        else:
            nombre = self.nombreArchivo.valor+'.html'

        f = open(f'{str(nombre)}','w')
        f.write('<html>\n')
       
        f.write("""
            <head>
            <link rel="stylesheet" href="estilo.css">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <meta charset="utf-8"/>
            </head>
         """)
        f.write('<body>\n')
        f.write('<h1 class="d-flex justify-content-center p-3 mb-2 bg-primary text-white">Tabla Simbolos Local</h1>\n')
        f.write('<table class="table">\n' )
        f.write('<thead class="thead-dark">\n')
        f.write('<tr>\n')
        th = """ 
            <th scope="col">No.</th>
            <th scope="col">tipo</th>
            <th scope="col">Nombre</th>
            <th scope="col">valor</th>
            <th scope="col">Linea</th>
            """
        f.write(th)
        f.write('</tr>\n')
        f.write('</thead>\n')
        cont = 1
        f.write('<tbody>')
        
        while(actual != None):
            for i in actual.tablaSimbolos:
                valor = actual.tablaSimbolos[i]
                f.write('<tr>\n')
                f.write('   <th scope="row">'+ str(cont) +'</th>\n')
                f.write('   <th>'+ str(valor.tipo) +'</th>\n')
                f.write('   <th>'+ str(valor.nombre) +'</th>\n')
                f.write('   <th>'+ str(valor.valor) +'</th>\n')
                f.write('   <th>'+ str(valor.linea) +'</th>\n')
                f.write('</tr>\n')
                cont += 1
              
            actual = actual.anterior
            actual = None
        f.write('</tbody>')
        f.write('</table>\n' )
        f.write('</body>\n')
        f.write('</html>\n')
        f.close()
        webbrowser.open_new_tab(nombre)
        