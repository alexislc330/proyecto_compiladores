from Interface.Instruccion import Instruccion



class While(Instruccion):
    def __init__(self, fila, columna, condicion,instrucciones):
        self.fila = fila
        self.columna = columna
        self.condicion = condicion
        self.instrucciones = instrucciones 

    def ejecutar(self, entorno):
   
        valor = self.condicion.ejecutar(entorno)
        
        while(valor):
            for ins in self.instrucciones:
                ins.ejecutar(entorno)
            valor = self.condicion.ejecutar(entorno) 
      
        
