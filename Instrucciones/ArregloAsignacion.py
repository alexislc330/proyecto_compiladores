from Entorno.Entorno import Entorno
from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class ArregloAsignacion(Instruccion):
    def __init__(self, fila, columna,identtificador, posicion,expr) -> None:
        self.fila = fila
        self.columna =  columna
        self.identificador =  identtificador
        self.posicion =  posicion
        self.expr = expr 

    def ejecutar(self,ent:Entorno):
        psicion = []
        for i in self.posicion:
            psicion.append(i.ejecutar(ent))
        variable:Simbolo =  ent.buscarSimbolo(str(self.identificador))
  
        if variable != None:
            if len(psicion) == 1:
               
                var =  self.expr.ejecutar(ent)
                ent.buscarSimbolo(self.identificador).valor[psicion[0]] = var
            elif len(psicion) == 2:
                var =  self.expr.ejecutar(ent)
                ent.buscarSimbolo(self.identificador).valor[psicion[0]][psicion[1]] = var
            elif len(psicion) == 3:
                var =  self.expr.ejecutar(ent)
                ent.buscarSimbolo(self.identificador).valor[psicion[0]][psicion[1]][psicion[2]] = var          