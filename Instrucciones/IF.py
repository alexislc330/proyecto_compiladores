from Interface.Instruccion import Instruccion
from Entorno.Entorno import Entorno
from Instrucciones.Return import Return

class IF(Instruccion):
    def __init__(self, fila, columna, condicion, instrucciones, lelseif, else_instr):
        self.fila = fila
        self.columna = columna
        self.condicion = condicion
        self.instrucciones = instrucciones
        self.lelseif = lelseif
        self.else_instr = else_instr

    def ejecutar(self, ent: Entorno):
        cicloCumplido = False
        cond = self.condicion.ejecutar(ent)

      
        if True:
            if bool(cond):
                entIf = Entorno(ent)
                for ins in self.instrucciones:
                    ret = ins.ejecutar(entIf)
                    if isinstance(ins, Return):
                            return ins

            else:
                if self.lelseif is not None:
                    for lelif in self.lelseif:
                        condElseIf = lelif.condicion.ejecutar(ent)
                       
                        if True:
                            if condElseIf:
                                cicloCumplido = True
                                entElseIf = Entorno(ent)
                                for elsif in lelif.instrucciones:
                                    ret = elsif.ejecutar(entElseIf)
                                   

                if self.else_instr is not None and not cicloCumplido:
                    entElse = Entorno(ent)
                    for e in self.else_instr:
                        ret = e.ejecutar(entElse)
                       
