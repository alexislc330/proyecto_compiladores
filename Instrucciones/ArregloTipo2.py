from  Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo
from Entorno.Entorno import Entorno
import copy

from Instrucciones.Tsimbolos import Tsimbolos

class ArregloTipo2(Instruccion):
    def __init__(self, fila, columna, dimensiones, tipo, arreglos, id) -> None:
        self.fila = fila
        self.columna = columna
        self.dimensiones = dimensiones 
        self.tipo = tipo
        self.arreglos = arreglos
        self.id = id
    def ejecutar(self, entorno:Entorno):
        arr = self.arreglos
        nSimbolo = Simbolo(self.tipo, str(self.id),arr,0)
        cSimbolo = entorno.nuevoSimbolo(nSimbolo)
        if cSimbolo is not None:

            return nSimbolo.valor
