from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Interface.Instruccion import Instruccion


class Declaracion(Instruccion):
    def __init__(self, fila, columna, identificador, tipo, expresion=None):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.expresion = expresion
        self.tipo = tipo

    def ejecutar(self, entorno: Entorno):

        if self.expresion == None:
            simbolo = Simbolo(self.tipo, str(
                self.identificador).lower(), None, self.fila)
            entorno.nuevoSimbolo(simbolo)
            return
        else:
            valor = self.expresion.ejecutar(entorno)
            # verificar que valor no sea un error o nulo (None)
            simbolo = Simbolo(self.tipo, str(
                self.identificador).lower(), valor, self.fila)
            
        if self.tipo.upper() == self.expresion.tipo.upper():
           entorno.nuevoSimbolo(simbolo)
 
        else: 
            print(f"[FatalError]->error de declaracion, fila:{str(self.fila)}")   
        
      