from Interface.Instruccion import Instruccion
from Entorno.Funcion import SimboloFuncion
from Entorno.Entorno import Entorno
from Entorno.Simbolo import Simbolo
from Instrucciones.Return import Return
from Instrucciones.Tsimbolos import Tsimbolos
from FuncionesNativas.AMinus import AMinus
from FuncionesNativas.Round import Round
from FuncionesNativas.Truncate import Truncate
from FuncionesNativas.AMayus import AMayus


# from Nativas.Round import Round


class LlamadaFuncion(Instruccion):
    def __init__(self, fila, columna, identificador, parametros):
        self.fila = fila
        self.columna = columna
        self.identificador = identificador
        self.parametros = parametros
        self.tipo = None

    def ejecutar(self, ent: Entorno):
        func: Simbolo = ent.buscarSimbolo(self.identificador)
        
        parametrosRecibidos = []

        if func is not None:
            if func.tipo == 'FUNC':
                funcion: SimboloFuncion = func.valor
                self.tipo =  funcion.tipo
                if self.parametros is not None:
                   
                    if len(self.parametros) == len(funcion.param):
                        cont = 0
                        for p in self.parametros:
                            param: Simbolo = p.ejecutar(ent)
                           
                            if p.tipo.upper() == funcion.param[cont].tipo.upper():
                               
                                parametrosRecibidos.append(
                                    Simbolo(p.tipo.upper(), funcion.param[cont].nombre, param))
                            else:
                                print("Tipo de parametro no coincide")
                                return None
                            cont += 1
                    else:
                        print("La cantidad de parametros no coincide")



                # creamos un entorno para la ejecucion de la funcion
                entFuncion = Entorno(ent)

           
                for p in parametrosRecibidos:
                    s = entFuncion.nuevoSimbolo(p)
                    if s is None:
                        print("No se pudo agregar parametro")
                        return None

                for ins in funcion.instrucciones:
                    r = ins.ejecutar(entFuncion)
                    if isinstance(ins, Return):
                        return r
                    if isinstance(r, Return):
                        return r.ejecutar(entFuncion)

              
        else:
            iden = str(self.identificador).lower()
            if iden == 'aminus':
                func: AMinus = AMinus(self.fila, self.columna, self.parametros)
                self.tipo = 'STRING'
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == 'round':
                func: Round = Round(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == "truncate":
                func: Truncate = Truncate(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r
            elif iden == "amayus":
                self.tipo = 'STRING'
                func: AMayus = AMayus(self.fila, self.columna, self.parametros)
                r = func.ejecutar(ent)
                if r is not None:
                    return r