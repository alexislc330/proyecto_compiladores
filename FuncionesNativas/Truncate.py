from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo
import math

class Truncate(Instruccion):
    def __init__(self, fila, columna, parametro):
        self.fila = fila
        self.columna = columna
        self.parametro = parametro

    def ejecutar(self, ent):
        if self.parametro is not None:
            if len(self.parametro) == 1:
                param: Simbolo = self.parametro[0].ejecutar(ent)
                if self.parametro[0].tipo == 'INT' or self.parametro[0].tipo == 'DOUBLE':
                    return  math.trunc(param) 
                else:
                    print("La funcion Truncate no admite parametros de tipo " + param.tipo)
            else:
                print("La funcion Truncate debe traer solamente un parametro")
        else:
            print("La funcion Truncate debe traer parametro")

        return None
