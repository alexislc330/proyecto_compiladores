from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo


class AMayus(Instruccion):
    def __init__(self, fila, columna, parametros):
        self.columna = columna
        self.fila = fila
        self.parametros = parametros

    def ejecutar(self, entorno):
        if self.parametros is not None:
            if len(self.parametros) == 1:
                param = self.parametros[0].ejecutar(entorno)
                if self.parametros[0].tipo.upper() == 'STRING':
                    return param.upper()
                elif self.parametros[0].tipo.upper() == 'CHAR':
                    return param.upper()
                else:
                    print("El parametro para la funcion aMayus debe ser de tipo STRING")
            else:
                print("La funcion aMayus debe traer solamente un parametro")
        else:
            print("La funcion aMayus debe traer un parametro")
        return None
