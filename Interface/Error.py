class Error:
    def __init__(self, tipo,descripcion, fila, columna) -> None:
        self.tipo = tipo
        self.descripcion = descripcion
        self.fila = fila
        self.columna = columna
    def toString(self):
        return "ERRO"+ self.tipo +": " +self.descripcion + ". En fila"+ " y columna" + str(self.columna)
    