from re import S
from Interface.Instruccion import Instruccion


class Aritmetica(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = None

    def ejecutar(self, entorno):
        izq = self.hijoIzq.ejecutar(entorno)
        der = self.hijoDer.ejecutar(entorno)
     
        if self.operador == '+':
            if self.hijoIzq.tipo.upper() == 'INT':
                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    return self.obtenerValor('INT', izq) + self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'STRING':
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('INT', izq)) + str(self.obtenerValor('STRING', der))

                elif self.hijoDer.tipo.upper() == 'BOOLEAN':
                    self.tipo =  "INT"
                    valorBool = self.obtenerValor("BOOLEAN",der)
                    return (1 if valorBool else 0) + self.obtenerValor('INT', izq)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) + self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'STRING':
           
                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('INT', der))

                elif self.hijoDer.tipo.upper() == 'STRING':
                    self.tipo  = "STRING" 
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('STRING', der))

                elif self.hijoDer.tipo.upper() == "DOUBLE":
                    self.tipo = "STRING"
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('DOUBLE', der))

                elif self.hijoDer.tipo.upper() == "BOOLEAN":
                    self.tipo = "STRING"
                    valorBool = self.obtenerValor("BOOLEAN",der)
                    return str(self.obtenerValor('STRING', izq)) + str(der)    

                elif self.hijoDer.tipo.upper() == "CHAR":
                    self.tipo = "STRING"
                    return str(self.obtenerValor('STRING', izq)) + str(self.obtenerValor('CHAR', der))


            elif self.hijoIzq.tipo.upper() == 'BOOLEAN':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = "INT"
                    valorBool = self.obtenerValor("BOOLEAN",izq)
                    return (1 if valorBool else 0) + self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = "DOUBLE"
                    valorBool = self.obtenerValor("BOOLEAN",izq)
                    return (1 if valorBool else 0) + self.obtenerValor('DOUBLE', der)

                elif self.hijoDer.tipo.upper() == 'BOOLEAN':
                    self.tipo = "INT"
                    valorBoolIz =  self.obtenerValor("BOOLEAN",izq)
                    valorBoolDer =  self.obtenerValor("BOOLEAN",der)
                    return (1 if valorBoolIz else 0) + (1 if valorBoolDer else 0) 

                elif self.hijoDer.tipo.upper() == 'STRING':
                    self.tip = "STRING"
                    valorBoolIz =  self.obtenerValor("BOOLEAN",izq)
                    return str(1 if valorBoolIz else 0) + str(self.obtenerValor('STRING', der))
            
            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) + self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'STRING':
                    self.tipo = 'STRING'
                    return str(self.obtenerValor('DOUBLE', izq)) + str(self.obtenerValor('STRING', der))

                elif self.hijoDer.tipo.upper() == 'BOOLEAN':
                    self.tipo =  "DOUBLE"
                    valorBool = self.obtenerValor("BOOLEAN",der)
                    return self.obtenerValor('DOUBLE', izq) + (1 if valorBool else 0) 

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) + self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'CHAR':

                if self.hijoDer.tipo.upper() == 'CHAR':
                    self.tipo = "STRING"
                    return str(self.obtenerValor('CHAR', izq)) + str(self.obtenerValor('CHAR', der))
                
                if self.hijoDer.tipo.upper() == 'STRING':
                    self.tipo = "STRING"
                    return str(self.obtenerValor('CHAR', izq)) + str(self.obtenerValor('STRING', der))


        elif self.operador == '-':

            if self.hijoIzq.tipo.upper() == 'INT':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    return self.obtenerValor('INT', izq) - self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = "DOUBLE"
                    return self.obtenerValor('INT', izq) - self.obtenerValor('DOUBLE', der)

                elif self.hijoDer.tipo.upper() == 'BOOLEAN':
                    self.tipo =  "INT"
                    valorBool = self.obtenerValor("BOOLEAN",der)
                    return (1 if valorBool else 0) + self.obtenerValor('INT', izq)

            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) - self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = "DOUBLE"
                    return self.obtenerValor('DOUBLE', izq) - self.obtenerValor('DOUBLE', der)
                
                elif self.hijoDer.tipo.upper() == 'BOOLEAN':
                    
                    self.tipo = "DOUBLE"
                    valorBoolDer =  self.obtenerValor("BOOLEAN",der)
                    return self.obtenerValor('DOUBLE', izq) - (1 if valorBoolDer else 0) 

            elif self.hijoIzq.tipo.upper() == 'BOOLEAN':
                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    valorBoolIz =  self.obtenerValor("BOOLEAN",izq)
                    return (1 if valorBoolIz else 0) - self.obtenerValor('INT', der)


                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = "DOUBLE"
                    valorBoolIz =  self.obtenerValor("BOOLEAN",izq)
                    return (1 if valorBoolIz else 0) - self.obtenerValor('DOUBLE', der)



        elif self.operador == "*":

            if self.hijoIzq.tipo.upper() == 'INT':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    return self.obtenerValor('INT', izq) * self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) * self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) * self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) * self.obtenerValor('DOUBLE', der)


                    
        elif self.operador == "/":

            if self.hijoIzq.tipo.upper() == 'INT':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) / self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) / self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) / self.obtenerValor('INT', der)
                    
                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) / self.obtenerValor('DOUBLE', der)

        elif self.operador == "**":

            if self.hijoIzq.tipo.upper() == 'INT':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'INT'
                    return self.obtenerValor('INT', izq) ** self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) ** self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) ** self.obtenerValor('INT', der)
                    
                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) ** self.obtenerValor('DOUBLE', der)   


        elif self.operador == "%":

            if self.hijoIzq.tipo.upper() == 'INT':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) % self.obtenerValor('INT', der)

                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('INT', izq) % self.obtenerValor('DOUBLE', der)

            elif self.hijoIzq.tipo.upper() == 'DOUBLE':

                if self.hijoDer.tipo.upper() == 'INT':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) % self.obtenerValor('INT', der)
                    
                elif self.hijoDer.tipo.upper() == 'DOUBLE':
                    self.tipo = 'DOUBLE'
                    return self.obtenerValor('DOUBLE', izq) % self.obtenerValor('DOUBLE', der)  
        

    def obtenerValor(self, tipo, valor):
        if tipo == 'INT':
            return int(valor)
        elif tipo == 'STRING':
            return str(valor)
        elif tipo == 'DOUBLE':
            return float(valor)
        elif tipo == 'BOOLEAN':
            return bool(valor)
        elif tipo == 'CHAR':
            return str(valor)
