from Entorno.Entorno import Entorno
from Interface.Instruccion import Instruccion
from Expresiones.Primitivo import Primitivo
class Arreglo(Instruccion):
    
    def __init__(self, fila, columna, identificador,  posicion) -> None:
        self.fila = fila 
        self.columna = columna
        self.identificador = identificador  
        self.posicion = posicion
        self.tipo = None

    def ejecutar(self, entorno:Entorno):
        pscion = []

        for i  in self.posicion: 
            pscion.append(i.ejecutar(entorno))

        r =  entorno.buscarSimbolo(self.identificador)
        self.tipo =  r.tipo
        if r is not None:
            if len(pscion) == 1:
                if isinstance(entorno.buscarSimbolo(self.identificador).valor[pscion[0]],Primitivo):
                    return entorno.buscarSimbolo(self.identificador).valor[pscion[0]].ejecutar(entorno)
                return entorno.buscarSimbolo(self.identificador).valor[pscion[0]]

            elif len(pscion) == 2:  
                if isinstance(entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]],Primitivo):
                    return entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]].ejecutar(entorno)
                return entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]]


            elif len(pscion) == 3:
                if isinstance(entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]][pscion[2]],Primitivo):
                    return entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]][pscion[2]].ejecutar(entorno)
                return entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]][pscion[2]]
            elif len(pscion) == 4:
                return entorno.buscarSimbolo(self.identificador).valor[pscion[0]][pscion[1]][pscion[2]][pscion[3]]