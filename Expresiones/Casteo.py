from Interface.Instruccion import Instruccion
from Entorno.Simbolo import Simbolo
from ply.yacc import restart


class Casteo(Instruccion):
    def __init__(self, fila, columna, tipo, expr):
        self.columna = columna
        self.fila = fila
        self.tipo = tipo
        self.expr = expr

    def ejecutar(self, ent):
        expresion: Simbolo = self.expr.ejecutar(ent)


   
        if expresion is not None:
            
            if self.tipo.upper() == 'INT':
                if self.expr.tipo == 'DOUBLE':
                    #return Simbolo('INT', '', int(expresion), 0)
                    return int(expresion)
                if self.expr.tipo == 'CHAR':
                    #return Simbolo('INT', '', ord(expresion), 0)
                    
                    return ord(expresion)
                if self.expr.tipo == 'STRING':
                    if str(expresion).isnumeric():
                        #return Simbolo('INT', '', int(expresion), 0)
                        return int(expresion)
                    else:
                        return Simbolo('ERROR', 'La cadena no es de tipo entero', None, 0)

            if self.tipo.upper() == 'STRING':
                if self.expr.tipo.upper()  == 'INT':
                    return str(expresion)
                if self.expr.tipo.upper()  == 'DOUBLE':
                    return str(expresion)
                if self.expr.tipo.upper()  == 'BOOLEAN':
                    return str(expresion)

            if self.tipo.upper() == 'DOUBLE':
                if self.expr.tipo.upper() == 'INT':
                    return float(expresion) 
                if self.expr.tipo.upper() == 'STRING':
                    return float(expresion)
                if self.expr.tipo.upper() == 'CHAR':
                    return float(ord(expresion))

            if self.tipo.upper() == 'CHAR':
                if self.expr.tipo.upper()  == 'INT':
                    return chr(expresion)
                if self.expr.tipo.upper()  == "DOUBLE":
                    return chr(int(expresion))

            if self.tipo.upper() == 'BOOLEAN':
                 if self.expr.tipo.upper()  == 'STRING':
                    if expresion.lower() == 'true':
                        return "true"
                    elif expresion.lower() == "false":
                        return "false"