from Interface.Instruccion import Instruccion


class Relacional(Instruccion):
    def __init__(self, fila, columna, operador, hijoIzq, hijoDer):
        self.fila = fila
        self.columna = columna
        self.operador = operador
        self.hijoIzq = hijoIzq
        self.hijoDer = hijoDer
        self.tipo = None
   
    def ejecutar(self, entorno):
        izq = self.hijoIzq.ejecutar(entorno)
        der = self.hijoDer.ejecutar(entorno)

        if self.operador == '<':
           
            return self.obtenerValor(self.hijoIzq.tipo.upper(), izq) < self.obtenerValor(self.hijoDer.tipo.upper(), der)

        if self.operador == '>':
   
            return self.obtenerValor(self.hijoIzq.tipo.upper(), izq) > self.obtenerValor(self.hijoDer.tipo.upper(), der)
        if self.operador == '<=':
   
            return self.obtenerValor(self.hijoIzq.tipo.upper(), izq) <= self.obtenerValor(self.hijoDer.tipo.upper(), der)
        if self.operador == '>=':
   
            return self.obtenerValor(self.hijoIzq.tipo.upper(), izq) >= self.obtenerValor(self.hijoDer.tipo.upper(), der)
        if self.operador == "==":
            return self.obtenerValor(self.hijoIzq.tipo.upper(),izq) == self.obtenerValor(self.hijoDer.tipo.upper(), der)
        
        if self.operador == "!=":
            return self.obtenerValor(self.hijoIzq.tipo.upper(),izq) != self.obtenerValor(self.hijoDer.tipo.upper(), der)   
        if izq == "1" and der is None:
            return True
            
        if izq == "0" and der is None:
            return False


    def obtenerValor(self, tipo, valor):
        if tipo == 'INT':
            return int(str(valor))
        elif tipo == 'STRING':
            return str(valor)
        elif tipo == 'DOUBLE':
            return float(str(valor))
        elif tipo == 'BOOLEAN':
            return bool(str(valor))
