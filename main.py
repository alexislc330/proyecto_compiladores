
import sys
from PyQt5 import QtCore, QtGui
from PyQt5.QtCore import Qt, QRect, QSize
from PyQt5.QtWidgets import QAction,QApplication,QFileDialog, QMainWindow,QWidget
from PyQt5.QtGui import QColor, QPainter, QTextFormat, QKeySequence, QFont
from editor_de_texto import editor_texto
from PlainTextEdit import PlainTextEdit
from Gramatica import gramatica



class MyStream(QtCore.QObject):
    message = QtCore.pyqtSignal(str)
    def __init__(self, parent=None):
        super(MyStream, self).__init__(parent)

    def write(self, message):
        self.message.emit(str(message))



class Editor_Texto(QMainWindow):
    def __init__(self):
        super().__init__()
        
        self.ui  = editor_texto()
        self.reporteEnt =  None
        self.ui.setupUi(self)
    
        self.ui.Btn_generar.clicked.connect(self.ejecutar)
        self.ui.Btn_reporte.clicked.connect(self.reporte)
        self.ui.actionAbrir.triggered.connect(self.abrir)
       
        self.show()
    def __del__(self):
    # Restore sys.stdout
        sys.stdout = sys.__stdout__ 


    @QtCore.pyqtSlot(str)
    def on_myStream_message(self, message):
        
        self.ui.Caja_de_texto_salida.insertPlainText(message)

    def reporte(self):
        actual = self.reporteEnt
        f = open('reporte.html','w')
        f.write('<html>\n')
       
        f.write("""
            <head>
            <link rel="stylesheet" href="estilo.css">
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <meta charset="utf-8"/>
            </head>
         """)
        f.write('<body>\n')
        f.write('<h1 class="d-flex justify-content-center p-3 mb-2 bg-primary text-white">Tabla Simbolos Global</h1>\n')
        f.write('<table class="table">\n' )
        f.write('<thead class="thead-dark">\n')
        f.write('<tr>\n')
        th = """ 
            <th scope="col">No.</th>
            <th scope="col">tipo</th>
            <th scope="col">Nombre</th>
            <th scope="col">valor</th>
            <th scope="col">Linea</th>
            """
        f.write(th)
        f.write('</tr>\n')
        f.write('</thead>\n')
        cont = 1
        f.write('<tbody>')
        
        while(actual != None):
            for i in actual.tablaSimbolos:
                valor = actual.tablaSimbolos[i]
                f.write('<tr>\n')
                f.write('   <th scope="row">'+ str(cont) +'</th>\n')
                f.write('   <th>'+ str(valor.tipo) +'</th>\n')
                f.write('   <th>'+ str(valor.nombre) +'</th>\n')
                f.write('   <th>'+ str(valor.valor) +'</th>\n')
                f.write('   <th>'+ str(valor.linea) +'</th>\n')
                f.write('</tr>\n')
                cont += 1
              
            actual = actual.anterior
        f.write('</tbody>')
        f.write('</table>\n' )
        f.write('</body>\n')
        f.write('</html>\n')
        f.close()
        
        import webbrowser
        webbrowser.open_new_tab('reporte.html')
    def abrir(self):
        archivo = QFileDialog.getOpenFileName(self, "Abrir Archivo","C:\\")

        if archivo[0]:
            with open(archivo[0], 'rt') as f:
                datos =  f.read()
                self.ui.Caja_de_texto_entrada.setText(datos)

    def ejecutar(self):
        contenido = self.ui.Caja_de_texto_entrada.toPlainText()
      
        ent = gramatica(contenido)
        self.reporteEnt =  ent

if __name__ == "__main__":
    app = QApplication(sys.argv)
    ventana = Editor_Texto() 
    ventana.show()
    myStream = MyStream()
    myStream.message.connect(ventana.on_myStream_message)

    sys.stdout = myStream        
    sys.exit(app.exec_())
   